package com.example.abdallaah.devslopesradio.Activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.abdallaah.devslopesradio.Fragments.DetailsFragment;
import com.example.abdallaah.devslopesradio.Fragments.MainFragment;
import com.example.abdallaah.devslopesradio.Model.Station;
import com.example.abdallaah.devslopesradio.R;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static MainActivity mainActivity;

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    public static void setMainActivity(MainActivity mainActivity) {
        MainActivity.mainActivity = mainActivity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivity.setMainActivity(this);

        Log.d(TAG, "onCreate: before fragmentManager");
        FragmentManager fragmentManager = getSupportFragmentManager();
        MainFragment mainFragment = (MainFragment) fragmentManager.findFragmentById(R.id.container_main);

        if(mainFragment == null){
            Log.d(TAG, "onCreate: mainFragment null");
            mainFragment = MainFragment.newInstance("sd", "ad");
            fragmentManager.beginTransaction().
                    add(R.id.container_main, mainFragment).
                    commit();
        }
        Log.d(TAG, "onCreate: ends");
    }

    public void loadDetailsScreen(Station selectedStation){
        getSupportFragmentManager().
                beginTransaction().
                replace(R.id.container_main, new DetailsFragment()).
                addToBackStack(null).
                commit();
    }
}
