package com.example.abdallaah.devslopesradio.Model;


import android.util.Log;

public class Station {
    private static final String TAG = "Station";

    private String stationTitle;
    private String imageUri;
    final String DRAWABLE = "drawable/";

    public Station(String stationTitle, String imageUri) {
        Log.d(TAG, "Station: starts constructor");
        this.stationTitle = stationTitle;
        this.imageUri = imageUri;
    }

    public String getStationTitle() {
        return stationTitle;
    }

    public String getImageUri() {
        return DRAWABLE + imageUri;
    }
}
