package com.example.abdallaah.devslopesradio.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abdallaah.devslopesradio.Activities.MainActivity;
import com.example.abdallaah.devslopesradio.Holders.StationViewHolder;
import com.example.abdallaah.devslopesradio.Model.Station;
import com.example.abdallaah.devslopesradio.R;

import java.util.ArrayList;

public class StationsAdapter extends RecyclerView.Adapter<StationViewHolder>{
    private static final String TAG = "StationsAdapter";
    private ArrayList<Station> stations;

    public StationsAdapter(ArrayList<Station> stations) {
        Log.d(TAG, "StationsAdapter: constructor starts");
        this.stations = stations;
        Log.d(TAG, "StationsAdapter: constructor  ends");
    }

    @Override
    public StationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View stationCard = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_station, parent, false);
        return new StationViewHolder(stationCard);
    }

    @Override
    public void onBindViewHolder(StationViewHolder holder, int position) {
        final Station station = stations.get(position);
        holder.updateUI(station);

        holder.itemView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MainActivity.getMainActivity().loadDetailsScreen(station);
                    }
                }
        );

    }

    @Override
    public int getItemCount() {

        return stations.size();
    }
}
