package com.example.abdallaah.devslopesradio.Fragments;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abdallaah.devslopesradio.Adapters.StationsAdapter;
import com.example.abdallaah.devslopesradio.R;
import com.example.abdallaah.devslopesradio.Services.DataService;

public class StationsFragment extends Fragment {
    private static final String TAG = "StationsFragment";

    private static final String ARG_STATION_TYPE = "station_type";

    public static final int STATION_TYPE_FEATURED = 0;
    public static final int STATION_TYPE_RECENT = 1;
    public static final int STATION_TYPE_PARTY = 2;

    private int stationType;


    public StationsFragment() {
    }

    public static StationsFragment newInstance(int stationType) {
        Log.d(TAG, "newInstance: starts");
        StationsFragment fragment = new StationsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_STATION_TYPE, stationType);
        fragment.setArguments(args);
        Log.d(TAG, "newInstance() returned: " + fragment);
        Log.d(TAG, "newInstance: ends");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            stationType = getArguments().getInt(ARG_STATION_TYPE);
        }
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: starts");
        View view =  inflater.inflate(R.layout.fragment_stations, container, false);

        RecyclerView recyclerView =  view.findViewById(R.id.recycler_stations);
        recyclerView.setHasFixedSize(true);

        StationsAdapter stationsAdapter;

        if(stationType == STATION_TYPE_FEATURED){
            stationsAdapter = new StationsAdapter(DataService.getInstance().getFeaturedStations());
        }

        else if(stationType == STATION_TYPE_PARTY){
            stationsAdapter = new StationsAdapter(DataService.getInstance().getPartyStations());
        }

        else {
            stationsAdapter = new StationsAdapter(DataService.getInstance().getRecentStations());
        }

        recyclerView.addItemDecoration(new HorizontalSpaceItemDecorator(30));
        recyclerView.setAdapter(stationsAdapter);

        //For inside the recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);


        Log.d(TAG, "onCreateView() returned: " + view);
        Log.d(TAG, "onCreateView: ends");
        return view;
    }


    class HorizontalSpaceItemDecorator extends RecyclerView.ItemDecoration{
        private final int spacer;

        public HorizontalSpaceItemDecorator(int spacer) {
            this.spacer = spacer;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.right = spacer;
        }
    }

}
