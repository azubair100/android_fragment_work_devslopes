package com.example.abdallaah.devslopesradio.Holders;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abdallaah.devslopesradio.Model.Station;
import com.example.abdallaah.devslopesradio.R;


public class StationViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = "StationViewHolder";

    private ImageView mainImage;
    private TextView titleTextView;


    public StationViewHolder(View itemView) {
        super(itemView);
        Log.d(TAG, "StationViewHolder: consttructor starts");
        this.mainImage = itemView.findViewById(R.id.main_image);
        this.titleTextView = itemView.findViewById(R.id.main_text);

        Log.d(TAG, "StationViewHolder: constructor ends");

    }

    //whenever you wanna update the UI with new data
    public void updateUI(Station station){
        Log.d(TAG, "updateUI: starts");

        String uri = station.getImageUri();
        int resource = mainImage.getResources().getIdentifier(uri, null, mainImage.getContext().getPackageName());
        mainImage.setImageResource(resource);

        titleTextView.setText(station.getStationTitle());

        Log.d(TAG, "updateUI: ends");
    }


}
