package com.example.abdallaah.devslopesradio.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abdallaah.devslopesradio.R;

public class MainFragment extends Fragment {
    private static final String TAG = "MainFragment";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;


    public MainFragment() {
    }

    public static MainFragment newInstance(String param1, String param2) {
        Log.d(TAG, "newInstance: starts");
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        Log.d(TAG, "newInstance() returned: " + fragment);
        Log.d(TAG, "newInstance: ends");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView: starts");
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        StationsFragment stationsFragment1;
        StationsFragment stationsFragment2;
        StationsFragment stationsFragment3;

        stationsFragment1 = StationsFragment.newInstance(StationsFragment.STATION_TYPE_FEATURED);
        fragmentManager.beginTransaction().add(R.id.container_top_row, stationsFragment1).commit();

        stationsFragment2 = StationsFragment.newInstance(StationsFragment.STATION_TYPE_PARTY);
        fragmentManager.beginTransaction().add(R.id.container_middle_row, stationsFragment2).commit();

        stationsFragment3 = StationsFragment.newInstance(StationsFragment.STATION_TYPE_RECENT);
        fragmentManager.beginTransaction().add(R.id.container_bottom_row, stationsFragment3).commit();

        Log.d(TAG, "onCreateView: ends");

        Log.d(TAG, "onCreateView() returned: " + view);
        return view;
    }

}
