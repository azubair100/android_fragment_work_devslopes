package com.example.abdallaah.devslopesradio.Services;

import android.util.Log;

import com.example.abdallaah.devslopesradio.Model.Station;

import java.util.ArrayList;

public class DataService {
    private static final DataService ourInstance = new DataService();
    private static final String TAG = "DataService";

    public static DataService getInstance() {
        return ourInstance;
    }

    private DataService() {
        Log.d(TAG, "DataService: constrcutor start and ends");
    }

    public ArrayList<Station> getFeaturedStations(){
        Log.d(TAG, "getFeaturedStations: starts");
        ArrayList<Station> list = new ArrayList<>();
        list.add(new Station("Bike", "me_blue_bike"));
        list.add(new Station("n3", "n3"));
        list.add(new Station("n4", "n4"));
        list.add(new Station("n5", "n5"));

        Log.d(TAG, "getFeaturedStations() returned: " + list);
        Log.d(TAG, "getFeaturedStations: ends");
        return list;
    }


    public ArrayList<Station> getPartyStations(){
        Log.d(TAG, "getPartyStations: starts");
        ArrayList<Station> list = new ArrayList<>();
        list.add(new Station("hikerswatch", "hikerswatch.jpg"));
        list.add(new Station("weather", "weather"));
        list.add(new Station("window", "window.jpg"));
        list.add(new Station("n4", "n4"));

        Log.d(TAG, "getPartyStations() returned: " + list);
        Log.d(TAG, "getPartyStations: ends");
        return list;
    }


    public ArrayList<Station> getRecentStations(){
        Log.d(TAG, "getRecentStations: starts");
        ArrayList<Station> list = new ArrayList<>();
        list.add(new Station("n5", "n5"));
        list.add(new Station("weather", "weather"));
        list.add(new Station("hikerswatch", "hikerswatch"));
        list.add(new Station("Bike", "me_blue_bike"));

        Log.d(TAG, "getRecentStations() returned: " + list);
        Log.d(TAG, "getRecentStations: ends");
        return list;
    }
}

